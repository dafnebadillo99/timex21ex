defmodule TimexWeb.IndigloManager do
  use GenServer
  def init(ui) do
    :gproc.reg({:p, :l, :ui_event})
    {:ok, %{ui_pid: ui, st: IndigloOff, count: 0}}
  end

  def handle_info(:"top-right", %{st: IndigloOff, ui_pid: ui} = state) do
    GenServer.cast(ui, :set_indiglo)
    {:noreply, %{state | st: IndigloOn} }
  end

  def handle_info(:"top-right", %{st: IndigloOn} = state) do
    Process.send_after(self(), Waiting2IndigloOff, 2000)
    {:noreply, %{state | st: Waiting} }
  end

  def handle_info(Waiting2IndigloOff, %{st: Waiting, ui_pid: ui} = state) do
    GenServer.cast(ui, :unset_indiglo)
    {:noreply, state |> Map.put(:st, IndigloOff)}
  end

  def handle_info(:alarm, %{st: st, count: count} = state) do
    _count = count + 1
    IO.inspect("ff")
    if st == IndigloOff do
      Process.send_after(self(), :alarm_off, 1000)
      {:noreply, %{state | st: IndigloOn}}
    else
      Process.send_after(self(), :alarm_on, 1000)
      {:noreply, %{state | st: IndigloOff}}
    end
    #{:noreply, state}
  end

  def handle_info(:alarm_off, %{st: IndigloOff, ui_pid: ui, count: count} = state) when count < 9 do
    Process.send_after(self(), :alarm_on, 1000)
    GenServer.cast(ui, :unset_indiglo)
    {:noreply, state |> Map.put(:count, count + 1) |> Map.put(:st, IndigloOn) }
   end

  def handle_info(:alarm_on, %{st: IndigloOn, ui_pid: ui, count: count} = state) do
    GenServer.cast(ui, :set_indiglo)
    if(count < 9) do
      GenServer.cast(ui, :set_indiglo)
      Process.send_after(self(), :alarm_off, 1000)
      {:noreply, state |> Map.put(:count, count + 1) |> Map.put(:st, IndigloOff) }
    else
      {:noreply, state |> Map.put(:count, 0) |> Map.put(:st, IndigloOff) }
    end
  end

  def handle_info(_msg, state) do
    {:noreply, state}
  end
end
